# -*- coding:utf-8 -*-
# self buy and sell
import time
import requests
from FCoinAPI import api_controller
import datetime
import math

f = open('apiKEY.txt', 'r')
apiKEY = f.readlines()[0]
f = open('apiSECRET.txt', 'r')
apiSECRET = f.readlines()[0]


api = api_controller(apiKEY, apiSECRET)

def get_ticker(symbol):
    r = requests.get('https://api.fcoin.com/v2/market/ticker/{}'.format(symbol))
    # print(r.json())
    max_buy = r.json()['data']['ticker'][2]
    min_sell = r.json()['data']['ticker'][4]
    # print(max_buy,min_sell)
    return max_buy, min_sell

t=0
while t<10:
    t1=time.time()
    get_ticker('ftusdt')
    t2 = time.time()
    print(str(t)+'        '+str(t2-t1))
    t=t+1